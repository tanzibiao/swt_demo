package com.tzb.inc.utils;

import java.io.InputStream;


public class ResUtil
{
    public static InputStream getRes(String path) {
        return ResUtil.class.getClassLoader().getResourceAsStream(path);
    }
}
