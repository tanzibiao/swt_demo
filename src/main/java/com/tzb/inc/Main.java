package com.tzb.inc;


import com.alibaba.fastjson.JSONObject;
import com.tzb.inc.utils.ResUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.program.Program;
import org.eclipse.swt.widgets.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import org.eclipse.wb.swt.SWTResourceManager;

@Slf4j
public class Main {

    protected Shell shell;
    private Text txtFileName;
    private Text txtArea;

    /**
     * Launch the application.
     *
     * @param args
     */
    public static void main(String[] args) {
        try {
            Main window = new Main();
            window.open();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Open the window.
     */
    public void open() {
        Display display = Display.getDefault();
        createContents();
        Image image = new Image(Display.getDefault(), ResUtil.getRes("static/icons/logo.png"));
        shell.setImage(image);
        shell.open();
        shell.layout();
        while (!shell.isDisposed()) {
            if (!display.readAndDispatch()) {
                display.sleep();
            }
        }
    }

    /**
     * Create contents of the window.
     */
    protected void createContents() {
        shell = new Shell();
        shell.setSize(373, 337);
        shell.setText("swt_demo-by tanzibiao");
        Menu menu = new Menu(shell, SWT.BAR);
        shell.setMenuBar(menu);
        MenuItem item = new MenuItem(menu, SWT.CASCADE);
        item.setText("清空日志");
        item.addSelectionListener(new SelectionListener() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                txtArea.setText("");
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {

            }
        });
        txtArea = new Text(shell, SWT.BORDER | SWT.READ_ONLY | SWT.WRAP | SWT.H_SCROLL | SWT.V_SCROLL | SWT.CANCEL | SWT.MULTI);
        txtArea.setForeground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
        txtArea.setBackground(SWTResourceManager.getColor(SWT.COLOR_BLACK));
        txtArea.setBounds(10, 10, 341, 119);
        
        Label labelTxt = new Label(shell, SWT.NONE);
        labelTxt.setBounds(10, 135, 65, 14);
        labelTxt.setText("统一前缀名");
        
        txtFileName = new Text(shell, SWT.BORDER);
        txtFileName.setText("重命名");
        txtFileName.setBounds(81, 133, 267, 27);
        
        Label label = new Label(shell, SWT.NONE);
        label.setBounds(10, 176, 21, 14);
        label.setText("方式：");

        Group groupEvn = new Group(shell, SWT.NONE);
        groupEvn.setBounds(37, 166, 122, 34);

        Button btnEvnTest = new Button(groupEvn, SWT.RADIO);
        btnEvnTest.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
            }
        });
        btnEvnTest.setText("方式1");
        btnEvnTest.setSelection(true);
        btnEvnTest.setBounds(10, 10, 48, 14);

        Button btnEvnPro = new Button(groupEvn, SWT.RADIO);
        btnEvnPro.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
            }
        });
        btnEvnPro.setText("方式2");
        btnEvnPro.setBounds(64, 10, 48, 14);

        Button btnChooseFile = new Button(shell, SWT.NONE);
        btnChooseFile.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                //检查加密后文件名
                String renameFileName = txtFileName.getText();
                if (StringUtils.isBlank(renameFileName)) {
                    pop("请填写统一前缀名");
                    return;
                }
                //选择文件
                //新建文件对话框，并设置为打开的方式
                //SWT.MULTI多选
                FileDialog filedlg = new FileDialog(shell, SWT.OPEN | SWT.MULTI);
                //设置文件对话框的标题
                filedlg.setText("选择文件");
                filedlg.open();
                //打开文件对话框，返回选中文件的绝对路径
                String path = filedlg.getFilterPath() + File.separator;
                String[] names = filedlg.getFileNames();
                if (names == null || names.length < 1) {
                    showLog("未选择文件");
                    return;
                }
                log.info("您选中的文件路径为：{}", path);
                showLog("您选中的文件路径为：{}", path);
                log.info("您选中的文件为：{}", JSONObject.toJSONString(names));
                showLog("您选中的文件为：{}", JSONObject.toJSONString(names));
                String renamePath = path + "rename" + File.separator;
                File file = new File(renamePath);
                if (!file.exists()) {
                    file.mkdirs();
                }
                for (String name : names) {
                    try {
                        Files.copy(new File(path+name).toPath(), new File(renamePath + renameFileName + "_" + name).toPath());
                    } catch (IOException ioException) {
                        showLog("拷贝文件发生异常");
                        ioException.printStackTrace();
                    }
                }
                showLog("更改文件名后路径：{}", renamePath);
                //Runtime.getRuntime().exec("explorer " + renamePath);
                //打开资源管理器
                Program.launch(renamePath);
            }
        });
        btnChooseFile.setBounds(37, 216, 311, 32);
        btnChooseFile.setText("选择文件");
    }

    /**
     * 显示日志到文本域，内容{}参数占位符
     * @author tanzibiao
     * @date 2021-12-18 09:25:49
     * @param msg
     * @param parm
     **/
    private void showLog(String msg, String... parm) {
        String s = msg.replaceAll("\\{\\}", "%s");
        this.txtArea.append(String.format(s, parm)+"\n");
    }

    /**
     * 弹框提示
     * @author tanzibiao
     * @date 2021-12-18 09:57:04
     * @param msg
     * @param parm
     **/
    private void pop(String msg, String... parm) {
        MessageBox mb = new MessageBox(shell,SWT.NONE);
        mb.setText("提示");
        String s = msg.replaceAll("\\{\\}", "%s");
        mb.setMessage(String.format(s, parm));
        //打开提示框
        mb.open();
    }
}
